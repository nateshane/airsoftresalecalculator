App = {
    load: function() {
        jo.load();

        document.body.addEventListener('touchmove', function(e) {
            e.preventDefault();
            joEvent.stop(e);
        }, false);

        this.scn = new joScreen(
            new joContainer([
                new joFlexcol([
                    this.stack = new joStackScroller()
                ])
            ]).setStyle({position: "absolute", top: "0", left: "0", bottom: "0", right: "0"})
        );

		this.stack.push(joCache.get("menu"));

		$('input').focus(function(){
			if($(this).val() == '0')
				$(this).val('');
		});

		$('input').blur(function(){
			if($(this).val() == '')
				$(this).val('0');
		});
    }
};